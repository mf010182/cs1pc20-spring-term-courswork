#include "Area.h"
#include <fstream>
#include <sstream>
#include <stdexcept>

void Area::AddRoom(const std::string &name, Room *room) { rooms[name] = room; }

Room *Area::GetRoom(const std::string &name) {
  auto it = rooms.find(name);
  if (it != rooms.end()) {
    return it->second;
  } else {
    return nullptr;
  }
}

void Area::ConnectRooms(const std::string &room1Name,
                        const std::string &room2Name,
                        const std::string &direction) {
  Room *room1 = GetRoom(room1Name);
  Room *room2 = GetRoom(room2Name);

  if (room1 && room2) {
    room1->AddExit(direction, room2);
    room2->AddExit(oppositeDirection(direction), room1);
  } else {
    throw std::runtime_error("One or both rooms not found while connecting.");
  }
}

void Area::LoadMapFromFile(const std::string &filename) {
  std::ifstream file(filename);
  if (!file.is_open()) {
    throw std::runtime_error("Unable to open file: " + filename);
  }

  std::string line;
  while (std::getline(file, line)) {
    std::istringstream iss(line);
    std::string room1Name;
    std::string direction;
    std::string room2Name;

    if (!(iss >> room1Name >> direction >> room2Name)) {
      throw std::runtime_error("Invalid format in map file.");
    }

    Room *room1 = GetRoom(room1Name);
    Room *room2 = GetRoom(room2Name);
    if (!room1) {
      room1 = new Room(room1Name);
      AddRoom(room1Name, room1);
    }
    if (!room2) {
      room2 = new Room(room2Name);
      AddRoom(room2Name, room2);
    }

    ConnectRooms(room1Name, room2Name, direction);
  }

  file.close();
}

std::string Area::oppositeDirection(const std::string &direction) {
  if (direction == "north")
    return "south";
  if (direction == "south")
    return "north";
  if (direction == "east")
    return "west";
  if (direction == "west")
    return "east";
  return ""; // Default case
}
