#include "Area.h"
#include "Player.h"
#include "Quest.h" // Assuming you have a Quest class defined in a separate file
#include <iostream>

void GameLoop(Player &player, Quest &rescueQuest, Quest &treasureQuest) {
  while (true) {
    std::cout << "Current Location: " << player.GetLocation()->GetDescription()
              << std::endl;
    std::cout << "Items in the room:" << std::endl;
    for (const Item &item : player.GetLocation()->GetItems()) {
      std::cout << "- " << item.GetName() << ": " << item.GetDescription()
                << std::endl;
    }
    std::cout << "Options: " << std::endl;
    std::cout << "1. Look around" << std::endl;
    std::cout << "2. Interact with an item" << std::endl;
    std::cout << "3. Move to another room" << std::endl;
    std::cout << "4. Check quests" << std::endl;
    std::cout << "5. Quit" << std::endl;

    int choice;
    std::cin >> choice;

    switch (choice) {
    case 1:
      std::cout << "You look around the room." << std::endl;
      break;
    case 2: {
      std::cout << "Enter the name of the item you want to interact with: ";
      std::string itemName;
      std::cin >> itemName;
      bool itemFound = false;
      for (const Item &item : player.GetLocation()->GetItems()) {
        if (item.GetName() == itemName) {
          item.Interact(); // Call Interact on the item found
          itemFound = true;
          break;
        }
      }
      if (!itemFound) {
        std::cout << "No such item found in the room." << std::endl;
      }
      break;
    }
    case 3: {
      std::cout << "Enter the direction (e.g., north, south): ";
      std::string direction;
      std::cin >> direction;
      Room *nextRoom = player.GetLocation()->GetExit(direction);
      if (nextRoom != nullptr) {
        player.SetLocation(nextRoom);
        std::cout << "You move to the next room." << std::endl;
      } else {
        std::cout << "You can't go that way." << std::endl;
      }
      break;
    }
    case 4:
      // Display quest descriptions and completion status
      std::cout << "Quests:" << std::endl;
      std::cout << "1. " << rescueQuest.GetDescription() << " - "
                << (rescueQuest.IsCompleted() ? "Completed" : "In progress")
                << std::endl;
      std::cout << "2. " << treasureQuest.GetDescription() << " - "
                << (treasureQuest.IsCompleted() ? "Completed" : "In progress")
                << std::endl;
      break;
    case 5:
      std::cout << "Goodbye!" << std::endl;
      return;
    default:
      std::cout << "Invalid choice. Try again." << std::endl;
      break;
    }
  }
}

int main() {
  // Create an instance of the Area class
  Area gameArea;

  try {
    // Load the game map from the text file
    gameArea.LoadMapFromFile("game_map.txt");
  } catch (const std::exception &e) {
    std::cerr << "Error: " << e.what() << std::endl;
    return 1; // Exit with error code
  }

  // Create quests
  Quest rescueQuest("Rescue the Captive");
  Quest treasureQuest("Find the Hidden Treasure");

  // Create a Player
  Player player("Jawad", 100);

  // Set the player's starting location
  player.SetLocation(gameArea.GetRoom("startRoom"));

  // Start the game loop
  GameLoop(player, rescueQuest, treasureQuest);

  return 0;
}
