// QuestManager.h
#ifndef QUEST_MANAGER_H
#define QUEST_MANAGER_H

#include <vector>
#include "Quest.h"

class QuestManager {
private:
    std::vector<Quest> quests;

public:
    void AddQuest(const Quest& quest);
    void RemoveQuest(const Quest& quest);
    void CheckQuestCompletion();
};

#endif // QUEST_MANAGER_H
