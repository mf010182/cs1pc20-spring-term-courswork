// Character.h
#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>

class Character {
private:
    std::string name;
    int health;

public:
    Character(const std::string& name, int health);

    void TakeDamage(int damage);
};

#endif // CHARACTER_H
