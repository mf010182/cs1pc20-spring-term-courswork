#include "Item.h"
#include <iostream>

Item::Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}

std::string Item::GetName() const {
    return name;
}

std::string Item::GetDescription() const {
    return description;
}

void Item::Interact() const {
    std::cout << "You interact with the " << name << ". " << description << std::endl;
}

bool Item::operator==(const Item& other) const {
    return name == other.name && description == other.description;
}
