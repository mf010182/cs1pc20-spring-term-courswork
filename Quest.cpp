// Quest.cpp
#include "Quest.h"

Quest::Quest(const std::string& desc) : description(desc), completed(false) {}

std::string Quest::GetDescription() const {
    return description;
}

bool Quest::IsCompleted() const {
    return completed;
}

void Quest::Complete() {
    completed = true;
}
