#include "Player.h"

// Constructor definition
Player::Player(const std::string& playerName, int playerHealth) : name(playerName), health(playerHealth), location(nullptr) {}

// Definition of GetLocation
Room* Player::GetLocation() const {
    return location;
}

// Definition of SetLocation
void Player::SetLocation(Room* room) {
    location = room;
}

// Definition of GetHealth
int Player::GetHealth() const {
    return health;
}

// Definition of SetHealth
void Player::SetHealth(int newHealth) {
    health = newHealth;
}
