// Quest.h
#ifndef QUEST_H
#define QUEST_H

#include <string>

class Quest {
private:
    std::string description;
    bool completed;
    // Other properties like rewards, conditions, etc.

public:
    Quest(const std::string& desc);

    std::string GetDescription() const;
    bool IsCompleted() const;
    void Complete();
};

#endif // QUEST_H
