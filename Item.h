#ifndef ITEM_H
#define ITEM_H

#include <string>

class Item {
private:
    std::string name;
    std::string description; // Add description member

public:
    Item(const std::string& itemName, const std::string& desc); // Update constructor
    std::string GetName() const;
    std::string GetDescription() const; // Add GetDescription member
    void Interact() const;

    bool operator==(const Item& other) const;
};

#endif // ITEM_H
