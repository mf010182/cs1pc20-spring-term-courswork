#ifndef PLAYER_H
#define PLAYER_H

#include "Room.h" // Include Room.h if needed

#include <string>

class Player {
private:
    std::string name;
    int health; // Add health member variable
    Room* location;

public:
    Player(const std::string& playerName, int playerHealth); // Constructor declaration
    Room* GetLocation() const; // Declaration of GetLocation
    void SetLocation(Room* room); // Declaration of SetLocation
    int GetHealth() const; // Declaration of GetHealth
    void SetHealth(int newHealth); // Declaration of SetHealth
    // Other member function declarations
};

#endif // PLAYER_H
